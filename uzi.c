#include "types.h"
#include "stat.h"
#include "user.h"
int
main(int argc, char *argv[])
{
  int i, n = 0;

  if(argc < 3) {
    printf(1, "Usage: sum a b c ...\n");
    exit();
  }

  for(i = 1; i < argc; i++) {
    n += atoi(argv[i]);
    printf(1, "%s%s", argv[i], i+1 < argc ? " + " : " = ");
  }
  printf(1, "%d\n", n);
  exit();
}
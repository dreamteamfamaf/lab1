#include "types.h"
#include "stat.h"
#include "user.h"
//Devuelve los valores de fibonacci hasta el numero dado
int
main(int argc, char *argv[]){
	if(argc != 2) {
    	printf(1, "Usage: fibo x ...\n");
    	exit();
  	}

	int array[atoi(argv[1])];
	array[0] = 1;
	array[1] = 1;
	for(int i = 2; i < atoi(argv[1]); i++){
		array[i] = array[i-2] + array[i-1];
	}

	for(int i = 0; i < atoi(argv[1]); i++){
		printf(1, "%d%s", array[i], i+1 < atoi(argv[1]) ? " " : "\n");
	}
  exit();
}
